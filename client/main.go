package main

import (
	"flag"
	"fmt"
	"net"
	"os"
)

var (
	CONN_HOST, CONN_PORT, CONN_TYPE, CONN_MSG, hosturl string
)

func main() {
	flag.StringVar(&CONN_HOST, "HOST", "localhost", "host for tcp server")
	flag.StringVar(&CONN_PORT, "PORT", "3333", "host for tcp server")
	flag.StringVar(&CONN_TYPE, "TYPE", "tcp", "host for tcp server")

	flag.Parse()

	hosturl = fmt.Sprintf("%v:%v", CONN_HOST, CONN_PORT)

	tcpServer, err := net.ResolveTCPAddr(CONN_TYPE, hosturl)

	if err != nil {
		println("ResolveTCPAddr failed:", err.Error())
		os.Exit(1)
	}

	conn, err := net.DialTCP(CONN_TYPE, nil, tcpServer)
	if err != nil {
		println("Dial failed:", err.Error())
		os.Exit(1)
	}

	_, err = conn.Write([]byte(CONN_MSG))
	if err != nil {
		println("Write data failed:", err.Error())
		os.Exit(1)
	}

	// buffer to get data
	received := make([]byte, 1024)
	_, err = conn.Read(received)
	if err != nil {
		println("Read data failed:", err.Error())
		os.Exit(1)
	}
	println("Response:", string(received))
}
