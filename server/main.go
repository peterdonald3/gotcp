package main

import (
	"flag"
	"fmt"
	"net"
	"os"
)

var (
	CONN_HOST, CONN_PORT, CONN_TYPE, hosturl string
)

func main() {
	flag.StringVar(&CONN_HOST, "HOST", "localhost", "host for tcp server")
	flag.StringVar(&CONN_PORT, "PORT", "3333", "host for tcp server")
	flag.StringVar(&CONN_TYPE, "TYPE", "tcp", "host for tcp server")

	flag.Parse()

	hosturl = fmt.Sprintf("%v:%v", CONN_HOST, CONN_PORT)
	l, err := net.Listen(CONN_TYPE, fmt.Sprintf("%v:%v", CONN_HOST, CONN_PORT))
	if err != nil {
		fmt.Printf("error listening: hostname - %v error - %v", hosturl, err.Error())
		os.Exit(1)
	}
	// Close the listener when the application closes.
	defer l.Close()
	fmt.Println("Listening on " + CONN_HOST + ":" + CONN_PORT)
	for {
		// Listen for an incoming connection.
		conn, err := l.Accept()
		if err != nil {
			fmt.Println("Error accepting: ", err.Error())
			os.Exit(1)
		}
		// Handle connections in a new goroutine.
		go handleRequest(conn)
	}
}

// Handles incoming requests.
func handleRequest(conn net.Conn) {
	// Make a buffer to hold incoming data.
	buf := make([]byte, 1024)
	// Read the incoming connection into the buffer.
	len, err := conn.Read(buf)
	if err != nil {
		fmt.Println("Error reading:", err.Error())
	}
	fmt.Printf("Message received: %s\n", string(buf[:len]))
	MSGREC := fmt.Sprintf("MsgRec: %v", string(buf[:len]))
	// Send a response back to person contacting us.
	conn.Write([]byte(MSGREC))
	// Close the connection when you're done with it.
	conn.Close()
}
